/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/tabs
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
(function( $ ) {
 
    $.fn.tabs = function(options) {

        var defaults = {   
            debug:      false,
            speed:      500
        };
     
        var settings = $.extend( {}, defaults, options );

        this.each(function () {

            var self = $(this);

            var getSpeed = function(obj) {
                var speed = settings.speed;
                if(obj.data('speed')) {
                    speed = obj.data('speed');
                }
                return speed;
            }

            var changeTab = function(obj) {
                var speed = getSpeed(self);

                self.find('li').removeClass('active');
                obj.addClass('active');

                self.find('.m-tab-content').hide();
                self.find(obj.data('target')).fadeIn(speed);
            }

            changeTab( $(this).find('li:first') );

            $(this).find('.m-tab-menu').find('li').off().on('click', function(e) {
                e.preventDefault();
                changeTab( $(this) );
            });
        });

        return this;
    };

}( jQuery ));